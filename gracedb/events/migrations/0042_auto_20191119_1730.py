# -*- coding: utf-8 -*-
# Generated by Django 1.11.23 on 2019-11-19 17:30
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0041_add_raven_voevent_fields'),
    ]

    operations = [
        migrations.AlterField(
            model_name='voevent',
            name='ivorn',
            field=models.CharField(blank=True, default='', editable=False, max_length=300),
        ),
    ]
